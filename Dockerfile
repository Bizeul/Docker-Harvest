#
# Dockerfile for build container
#
# Run docker build . in the current directory to build a container used to build
# the virtual machine and run unit tests


FROM alpine:latest

LABEL maintainer "Yann Bizeul (yann@tynsoe.org)"

RUN apk add --update perl perl-json perl-lwp-protocol-https perl-xml-parser

RUN mkdir -p /opt/netapp-harvest/lib

RUN echo "/opt/netapp-harvest/netapp-manager -start && tail -f /opt/netapp-harvest/log/*.log" > /start.sh && chmod 755 /start.sh

COPY files/lib/perl/NetApp /opt/netapp-harvest/lib

COPY files/netapp-harvest /opt/netapp-harvest

CMD /start.sh

VOLUME /opt/netapp-harvest/netapp-harvest.conf