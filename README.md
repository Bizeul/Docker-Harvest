## Purpose

[NetApp Harvest](http://mysupport.netapp.com/tools/info/ECMLP2314554I.html?productID=61924) is an amazing performance monitoring tool available from NetApp TooChest, but not directly available as a Docker container.

This projects aims at providing an easy way to build a NetApp Harvest container with minimum hassle.

## How to build a container

### Collect necessary materials

Because of licensing issues, it is not possible to distribute software components that are NetApp property, this is why you must login on NetApp support site with your own account and download the following files :

- [NetApp SDK](http://mysupport.netapp.com/NOW/cgi-bin/software?product=NetApp+Manageability+SDK&platform=All+Platforms) is available from the download page on NetApp web site
- [NetApp Harvest](http://mysupport.netapp.com/tools/info/ECMLP2314554I.html?productID=61924) is available from ToolChest

### Prepare the environment

Once you downloaded these two file in the current directory, you can run the `prebuild` script to uncompress the necessary components.

Example :
```
$ ls -l
-rw-r--r--@ 1 yann  staff       636 Jun 28 12:23 Dockerfile
-rw-r--r--@ 1 yann  staff      1653 Jun 28 14:04 README.md
-rw-r--r--@ 1 yann  staff    370047 Jun 27 18:41 netapp-harvest-1.3.zip
-rw-r--r--@ 1 yann  staff  67441710 Jun 28 09:44 netapp-manageability-sdk-5.5.zip
-rwxr-xr-x  1 yann  staff       267 Jun 28 10:29 prebuild

$ ./prebuild 
Archive:  ../netapp-manageability-sdk-5.5.zip
   creating: netapp-manageability-sdk-5.5/lib/perl/NetApp/
  inflating: netapp-manageability-sdk-5.5/lib/perl/NetApp/DfmErrno.pm  
  inflating: netapp-manageability-sdk-5.5/lib/perl/NetApp/NaElement.pm  
  inflating: netapp-manageability-sdk-5.5/lib/perl/NetApp/NaErrno.pm  
  inflating: netapp-manageability-sdk-5.5/lib/perl/NetApp/NaServer.pm  
  inflating: netapp-manageability-sdk-5.5/lib/perl/NetApp/ONTAPILogParser.pm  
  inflating: netapp-manageability-sdk-5.5/lib/perl/NetApp/ONTAPITestContainer.pm  
  inflating: netapp-manageability-sdk-5.5/lib/perl/NetApp/SdkEnv.pm  
  inflating: netapp-manageability-sdk-5.5/lib/perl/NetApp/Test.pm  
  inflating: netapp-manageability-sdk-5.5/lib/perl/NetApp/OCUMAPI.pm  
  inflating: netapp-manageability-sdk-5.5/lib/perl/NetApp/OCUMClassicAPI.pm  
  inflating: netapp-manageability-sdk-5.5/lib/perl/NetApp/Ontap7ModeAPI.pm  
  inflating: netapp-manageability-sdk-5.5/lib/perl/NetApp/OntapClusterAPI.pm  
Archive:  ../netapp-harvest-1.3.zip
  inflating: netapp-harvest-1.3.tgz  
```

You will then get a `files/` directory that contains part of NetApp SDK and NetApp Harvest that will be included in the container.

### Build the docker image

To build the image, run the following command :

`docker build -t yourname/netapp-harvest:latest .`

### Running the container

The image you get from the previous step can be pushed to any docker registry.

To run a container, you simply need to map the `netapp-harvest.conf` file with a volume mapping :

`docker run -d -v /path/to/netapp-harvest.conf:/opt/netapp-harvest/netapp-harvest.conf yourname/netapp-harvest:latest`